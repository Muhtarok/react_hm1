import PropTypes from 'prop-types';
import styles1 from './Buttons.module.scss'

// const Button1 = ({ color = "white", type = 'button', className = "button1", onClick = () => {}, children }) => {
const Button = ({ type, className, onClick, children }) => {

    return (

        <button type={type} className={styles1.button1} onClick={onClick}>
            {children}
        </button>
    )

    /*  const Button = ({ type, classNames, onClick, children }) => {
         return (
           <button type={type} className={`button ${classNames}`} onClick={onClick}>
             {children}
           </button>
         );
       }; */

};

Button.propTypes = {
    // color: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Button;






/* const Button = ({ color = 'green', onClick = () => {}, children }) => {
    
    return (
        <button style={{ background: color }} onClick={onClick}>{ children }</button>
    );
};

// Button.defaultProps = {
//     color: 'green', 
//     onClick: () => { console.log(1) }, 
// };

Button.propTypes = {
    color: PropTypes.string, 
    onClick: PropTypes.func, 
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
} */

