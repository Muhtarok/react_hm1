import PropTypes from 'prop-types';
/* import Button1 from "../Buttons/Buttons/button1.jsx";
import Button2 from "../Buttons/Buttons/button2.jsx"; */
import styles from './Modal.module.scss';
import ModalClose from './Modal/ModalClose';
import ModalFooter from './Modal/ModalFooter';

const Modal = ({ children, onClose }) => {

    const handleFirstButtonClick = () => {
        {isOpenModal1 && (
            <Modal onClose={closeModal1}>
              <h2>Modal 1 Content</h2>
              {/* <button onClick={() => setIsOpenModal1(false)}>Close</button> */}
            </Modal>
          )}
    };

    const handleSecondaryButtonClick = () => {
        console.log('Другий клік!');
    };

    return (
        <div className={styles.modalBackground} onClick={onClose}>
            <div className={styles.modalContent} onClick={(e) => e.stopPropagation()}>
                {/* <ModalClose></ModalClose> */}
                <ModalFooter
                    firstText="NO, CANCEL"
                    firstClick={handleFirstButtonClick}
                    secondaryText="YES, DELETE"
                    secondaryClick={handleSecondaryButtonClick} />

                {children}
            </div>
        </div>
    );
};

Modal.propTypes = {
    // color: PropTypes.string,
    onClose: PropTypes.func,
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Modal;

/* const ModalLaunch = ({ setIsOpenModal1 = () => {{console.log("hren")}}, setIsOpenModal2 = () => {}, isOpenModal1 = false, isOpenModal2 = false }) => { */

/*  const launchModal = () => {

 } */

/*  return (
     <div>
         <Button1 onClick={() => { setIsOpenModal1(true) }} >Open first modal</Button1>
         <Button2 onClick={() => { setIsOpenModal2(true) }} >Open second modal</Button2>
         <p>{console.log("loh")}</p>


     </div>
 ) */


/* } */

/* ModalLaunch.propTypes = {
    setIsOpenModal1: PropTypes.func,
    setIsOpenModal2: PropTypes.func.isRequired,
    isOpenModal1: PropTypes.bool,
    isOpenModal2: PropTypes.bool,

} */




/* return (
    <>
      <button onClick={() => { setIsOpen(true) }}>Turn On</button>


      {isOpen && (
        <div className='generalModal'>
          <div className='content'>
            <h1>Hello, i am a modal</h1>
          </div>

          <div onClick={() => { setIsOpen(false) }} className='background' />
        </div>
      )}
    </>
  ) */