import PropTypes from 'prop-types';
import styles from '../Modal.module.scss';
import CloseButtonSvg from './svg/index.jsx'

const ModalClose = ({ onClick, className }) => {

    return (
        <CloseButtonSvg className={styles.closeModalButton} onClick={onClick}></CloseButtonSvg>
    )
};

ModalClose.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
}

export default ModalClose;