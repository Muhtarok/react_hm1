import PropTypes from 'prop-types';
// import '../ButtonLaunchModal/Modal.scss';
import styles from '../../ButtonLaunchModal/Modal.module.scss';

const ModalFooter = ({ firstText="", secondaryText="", firstClick, secondaryClick }) => {

    return (
        <div className={styles.footer}>
            <button className={styles.modalButton} firstClick={firstClick}>
                {firstText}
            </button>

            <button className={styles.modalButton2} secondaryClick={secondaryClick}>
                {secondaryText}
            </button>
        </div>
    )
};

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
}

export default ModalFooter;