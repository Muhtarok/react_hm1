import PropTypes from 'prop-types';
// import '../ButtonLaunchModal/Modal.scss';
import styles from '../../ButtonLaunchModal/Modal.module.scss';

const ModalHeader = ({ children }) => {

    return (
        <div className={styles.footer}>
            <button className={styles.modalButton} firstClick={firstClick}>
                {firstText}
            </button>

            <button className={styles.modalButton2} secondaryClick={secondaryClick}>
                {secondaryText}
            </button>
        </div>
    )
};

ModalHeader.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default ModalFooter;