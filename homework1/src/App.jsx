import { useState } from 'react'
import './App.css'
import Button from "./components/Buttons/Buttons/TestButton.jsx"
import Modal from "./components/ButtonLaunchModal/ModalLaunch.jsx"
import './App.scss'
import ModalFooter from './components/ButtonLaunchModal/Modal/ModalFooter.jsx';

function App() {

  const [isOpenModal1, setIsOpenModal1] = useState(false);
  const [isOpenModal2, setIsOpenModal2] = useState(false);
  // const [firstClick, setfirstClick] = useState(false);

  const closeModal1 = () => {
    setIsOpenModal1(false);
  }

  const closeModal2 = () => {
    setIsOpenModal2(false);
  }

  const handleFirstButtonClick = () => {
    setIsOpenModal1(false);
  };

  const handleSecondaryButtonClick = () => {
      console.log('Другий клік!');
  };

  return (
    <main>
      <div className="app">
        {(!isOpenModal1 && !isOpenModal2) && (
          <Button onClick={() => setIsOpenModal1(true)}>Open first modal</Button>
        )}

        {(!isOpenModal1 && !isOpenModal2) && (
          <Button onClick={() => setIsOpenModal2(true)}>Open second modal</Button>
        )}

        {isOpenModal1 && (
          <Modal onClose={closeModal1}>
            <h2>Modal 1 Content</h2>
            <button onClick={() => setIsOpenModal1(false)}>Close</button>
          </Modal>
        )}

        {isOpenModal2 && (
          <Modal onClose={closeModal2}>
            <h2>Modal 2 Content</h2>
            <button onClick={() => setIsOpenModal2(false)}>Close</button>
          </Modal>
        )}
      </div>
    </main>
  );

}

export default App;
// const [count, setCount] = useState(0)
/* const [isOpenModal1, setIsOpenModal1] = useState(false);
const [isOpenModal2, setIsOpenModal2] = useState(false);

const test = () => {
  {(isOpenModal1 || isOpenModal2) && (
    <div className='generalModal'>
        <div className='content'>
            <h1>Hello, i am a modal</h1>
        </div>
        
        <div onClick={() => { setIsOpenModal1(false) }} className='background' />
        <div onClick={() => { setIsOpenModal2(false) }} className='background' />
    </div>
)}
}

return (
  <div>
    <p className="test">test</p>
      <ModalLaunch></ModalLaunch>
  </div>
) */


